//
//  HistoryTableViewCell.swift
//  OCRScanner
//
//  Created by Chính Trình Quang on 2/12/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTimeSaving: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setCell(history : HistoryItem) -> Void {
        
        if let time = history.timeSaving, let content = history.content{
            print(time)
            self.lblTimeSaving.text = time
            self.lblContent.text = content
        }
        
    }
    
}
