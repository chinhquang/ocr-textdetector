//
//  HistoryViewController.swift
//  OCRScanner
//
//  Created by Chính Trình Quang on 2/12/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit
import ReSwift
class HistoryViewController: UIViewController {
    var history = [HistoryItem]()
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.register(UINib(nibName: "HistoryTableViewCell", bundle: nil), forCellReuseIdentifier: "HistoryTableViewCell")
            tableView.delegate = self
            tableView.dataSource = self
            
            
            tableView.rowHeight = 200
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBar(forTittle: "History")
      
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.resetNavController()
        historyStore.subscribe(self)
        HistoryDataStore.sharedInstance.fetchHistoryFromCoreData(fromEntityName: "History")
        historyStore.dispatch(HistoryActionSetList(history: HistoryDataStore.sharedInstance.getHistoryList()))
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        historyStore.unsubscribe(self)
    }
}
extension HistoryViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DetectTextViewController.textShow  = history[indexPath.row].content!
        self.pushView(to: DetectTextViewController())
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            print(history[indexPath.row].id as Any)
            HistoryDataStore.sharedInstance.deleteData(id: history[indexPath.row].id!, entityname: "History")
           
            historyStore.dispatch(
                HistoryActionDelete(historyItem: history[indexPath.row])
            )
            tableView.reloadData()
        }
    }
}
extension HistoryViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return history.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell") as? HistoryTableViewCell
            else {
                return UITableViewCell()
        }
        
        cell.setCell(history: history[indexPath.row])
        print(history[indexPath.row].id as Any)
        return cell
    }
    
    
}
extension HistoryViewController: StoreSubscriber{
    
    func newState(state: HistoryState) {
        print("New state")
        
        history = state.histories
        tableView.reloadData()
    }
}
