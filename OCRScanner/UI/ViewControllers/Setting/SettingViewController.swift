//
//  SettingViewController.swift
//  OCRScanner
//
//  Created by Chính Trình Quang on 2/12/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit
import ReSwift
class SettingViewController: UIViewController, StoreSubscriber {
    var setting : Settings = Settings()
    func newState(state: SettingState) {
        setting = state.setting
    }
    
    
    @IBOutlet weak var switchSaveHistory: UISwitch!
    @IBOutlet weak var fontSizeBar: UISlider!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpView()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        settingStore.subscribe(self)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        settingStore.unsubscribe(self)
    }
    @IBAction func switchEnableHistory(_ sender: Any) {
        setting.enableSavingHistory = switchSaveHistory.isOn
        SettingDataStore.sharedInstance.updateSetting(newSetting: setting)
        settingStore.dispatch(SettingActionUpdate(settings: setting))
    }
    @IBAction func fontsizeChange(_ sender: UISlider) {
      
        let x : Double = Double(fontSizeBar.value).rounded(toPlaces: 1)
        setting.fontSize = x
        SettingDataStore.sharedInstance.updateSetting(newSetting: setting)
        settingStore.dispatch(SettingActionUpdate(settings: setting))
    }
    func setUpView(){
        self.setNavigationBar(forTittle: "Settings")
        
        
        self.fontSizeBar.tintColor = UIColor.blue
       
        
    }

}
