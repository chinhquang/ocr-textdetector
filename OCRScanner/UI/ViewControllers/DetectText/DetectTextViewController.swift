//
//  DetectTextViewController.swift
//  OCRScanner
//
//  Created by Chính Trình Quang on 2/13/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit
import ReSwift
import CoreData
class DetectTextViewController: UIViewController {
    var objects = [DetectObject]()
    var setting  = Settings()
    static var textShow : String = ""
    static var currentTime : String = {
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day ,.hour,.minute,.second], from: date)
        
        
        let year = String(components.year!)
        let month = String(components.month!)
        let day = String(components.day!)
        let hour = String(components.hour!)
        let minute = String(components.minute!)
        let second = String(components.second!)
        return year + "-" + month + "-" + day + "|"
            + hour + ":" + minute + ":" + second
    }()

    @IBOutlet weak var btnSaveHistory: UIButton!
    @IBOutlet weak var wordFrame: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
        setRightBarButton()
        UI.addDoneButton(controls: [wordFrame])
        detectObjectStore.subscribe(self)
        wordFrame.isEditable = false
        setting = SettingDataStore.sharedInstance.getSetting()
        wordFrame.font = UIFont(name: (wordFrame.font?.fontName)!, size: CGFloat(setting.fontSize!))
        wordFrame.text = DetectTextViewController.textShow
        btnSaveHistory.isHidden = !setting.enableSavingHistory!
        btnSaveHistory.isEnabled = setting.enableSavingHistory!
        
    }
    
    var item : HistoryItem = {
        let uuid = NSUUID().uuidString
        let time = DetectTextViewController.currentTime
        let content =  DetectTextViewController.textShow
        return HistoryItem(id: uuid, content: content, timesaving: time)
    }()
    fileprivate func setRightBarButton(){
        let editButton = UIButton(type: .system)
        editButton.setImage(#imageLiteral(resourceName: "icons8-pencil-60"), for: .normal)
        editButton.addTarget(self, action: #selector(editable), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: editButton)
    }
    @objc func editable(){
        var alert = UIAlertController()
        if wordFrame.isEditable == true {
            alert = UIAlertController(title: "Unable editing", message: "", preferredStyle: .alert)
            
        }else {
            alert = UIAlertController(title: "Enable editing", message: "", preferredStyle: .alert)
            
        }
        alert.view.tintColor = UIColor.black
        alert.addAction(UIAlertAction(title: "OK", style: .default) { [unowned self]  result in
            self.wordFrame.isEditable = !self.wordFrame.isEditable
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { [unowned self]  result in
            
        })
        self.present(alert, animated: true, completion: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super .viewWillDisappear(animated)
        detectObjectStore.unsubscribe(self)
        self.removeFromParent()
    }
    @IBAction func saveHistory(_ sender: Any) {
        
        let alert = UIAlertController(title: "Save this text to history", message: "", preferredStyle: .alert)
        alert.view.tintColor = UIColor.blue
        alert.addAction(UIAlertAction(title: "OK", style: .default) { [unowned self]  result in
            HistoryDataStore.sharedInstance.saveRecord(entityname: "History", newHistory: self.item)
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { [unowned self]  result in
            
        })
        present(alert, animated: true, completion: nil)
        
    }
   
    func reloadData()  {
         detectObjectStore.dispatch(DetectObjectActionSetList(objects: DetectObjectDataStore.sharedInstance.getListObject()))
    }

}
extension DetectTextViewController: StoreSubscriber{
    func newState(state: DetectObjectState) {
        objects = state.objects
        
       
    }
    
}
