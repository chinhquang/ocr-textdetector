//
//  CameraViewController.swift
//  OCRScanner
//
//  Created by Chính Trình Quang on 2/11/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit
import ReSwift
import Alamofire
class CameraViewController: UIViewController,UINavigationControllerDelegate{
    @IBOutlet weak var containerView: UIView!
    var imagePicker: UIImagePickerController!
    var setting : Settings = Settings()
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imageTake: UIImageView!
    enum ImageSource {
        case photoLibrary
        case camera
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    fileprivate func startIndicator (){
        activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        
    }
    fileprivate func stopIndicator (){
        activityIndicator.isHidden = true
        self.activityIndicator.stopAnimating()
        
    }

    fileprivate func showDialog(){
        let alert = UIAlertController(title: "Select image to detect text", message: "", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        alert.addAction(UIAlertAction(title: "Snap shot", style: .default) { [unowned self]  result in
            
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
            
        })
        
        alert.addAction(UIAlertAction(title: "Existing image", style: .default) { [unowned self]  result in
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
            
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive) { [unowned self]  result in
            
        })
        present(alert, animated: true, completion: nil)
        
        

    }
    
    @IBAction func analyzeImage(_ sender: Any) {
        DetectTextViewController.textShow = ""
        detectObjectStore.dispatch(DetectObjectActionRemoveAll())
        DetectObjectDataStore.sharedInstance.removeAll()
        self.startIndicator()
        DispatchQueue.main.async {
            let x = GoogleCloudOCR()
            guard let resizedImage = self.resize(image: self.imageTake.image!, to: self.imageTake.frame.size) else {
                fatalError("Error resizing image")
            }
            
            x.detect(from: resizedImage){
                result in
                print(result)
                guard let response = result["responses"] as? NSArray else {
                    
                    return
                    
                }
                
                if let x = response[0] as? [String: Any]{
                    if let textAnnotations = x["textAnnotations"] as? NSArray{
                        if textAnnotations.count == 0
                        {
                            let alert = UIAlertController(title: "No text found in this image", message: "", preferredStyle: .actionSheet)
                            alert.view.tintColor = UIColor.black
                            alert.addAction(UIAlertAction(title: "OK", style: .destructive) { [unowned self]  result in
                                self.stopIndicator()
                            })
                            self.present(alert, animated: true, completion: nil)
                        }else {
                            
                            for textAnnotation in textAnnotations{
                                if let obj = DetectObject(JSON: textAnnotation as! [String:Any]){
                                    //print(obj.description)
                                    DetectTextViewController.textShow += obj.description!
                                    DetectObjectDataStore.sharedInstance.add(newValue: obj)
                                    detectObjectStore.dispatch(
                                        DetectObjectActionAdd(object: obj)
                                    )
                                }
                            }
                            self.stopIndicator()
                            self.pushView(to: DetectTextViewController())
                        }
                        
                        
                    }else {
                        
                        
                        let alert = UIAlertController(title: "No text found in this image", message: "", preferredStyle: .actionSheet)
                        alert.view.tintColor = UIColor.black
                        alert.addAction(UIAlertAction(title: "OK", style: .destructive) { [unowned self]  result in
                            self.stopIndicator()
                        })
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
                
                
            }
            
        }
        

    }
    @IBAction func selectImage(_ sender: Any) {

        showDialog()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.resetNavController()
        settingStore.subscribe(self)
        setupView()
        
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        settingStore.unsubscribe(self)
        

    }
    private func resize(image: UIImage, to targetSize: CGSize) -> UIImage? {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle.
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height + 1)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    fileprivate func setupView (){
        self.setNavigationBar(forTittle: "Select Image")
        imageTake.layer.masksToBounds = true
        imageTake.layer.borderColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
        imageTake.layer.borderWidth = 3
        imageTake.cornerRadius = 10
        activityIndicator.isHidden = true
    }
}
extension CameraViewController: UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        imagePicker.dismiss(animated: true, completion: nil)
        guard let selectedImage = info[.originalImage] as? UIImage else {
            print("Image not found!")
            return
        }
       imageTake.image = selectedImage
    }
    
    

}
extension CameraViewController : StoreSubscriber{
    func newState(state: SettingState) {
        setting = state.setting
    }
}
