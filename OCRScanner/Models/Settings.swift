//
//  DetectedText.swift
//  OCRScanner
//
//  Created by Chính Trình Quang on 2/13/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
class  Settings {
    var language : String?
    var fontSize : Double?
    var enableSavingHistory : Bool?
    init(){
        self.language = "eng"
        self.enableSavingHistory = true
        self.fontSize = 15
    }
    init(lang : String, fontSize : Double, is_enableHistory :  Bool){
        self.language = lang
        self.enableSavingHistory = is_enableHistory
        self.fontSize = fontSize
    }
    
}
