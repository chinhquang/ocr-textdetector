//
//  Language.swift
//  OCRScanner
//
//  Created by Chính Trình Quang on 2/13/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
class Language{
    var code : String?
    var name : String?
    init() {
        self.code = "eng"
        self.name = "English"
    }
    init(code : String, name : String) {
        self.code = code
        self.name = name
    }
}
