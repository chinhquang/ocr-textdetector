//
//  HistoryItem.swift
//  OCRScanner
//
//  Created by Chính Trình Quang on 2/12/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
class HistoryItem {
    var id: String?
    var content: String?
    var timeSaving: String?
    
    init() {
       
    }
    
    init(id: String, content: String, timesaving: String) {
        self.id = id
        self.content = content
        self.timeSaving = timesaving
    }
}
