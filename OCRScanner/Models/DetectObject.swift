//
//  DetectObject.swift
//  OCRScanner
//
//  Created by Chính Trình Quang on 2/15/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import ObjectMapper
class DetectObject : Mappable{
    var description : String?
   
    
    init() {
        
    }
    required init?(map: Map){
        
    }
    func mapping(map: Map) {
        description <- map["description"]
        
        
    }
}
