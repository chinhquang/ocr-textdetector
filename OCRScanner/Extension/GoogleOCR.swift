//
//  GoogleOCR.swift
//  OCRScanner
//
//  Created by Chính Trình Quang on 2/15/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import Alamofire

class GoogleCloudOCR {
    private let apiKey = "AIzaSyCOPRYcMvUCduB1IP-yuy6oXVokQZpMjl0"
    private var apiURL: URL {
        return URL(string: "https://vision.googleapis.com/v1/images:annotate?key=\(apiKey)")!
    }
   
    func detect(from image: UIImage, completion: @escaping ([String:Any]) -> Void) {
        

        guard let base64Image = base64EncodeImage(image) else {
            print("Error while base64 encoding image")
            
            return
        }
        callGoogleVisionAPI(with: base64Image, completion: completion)
    }
    
    private func callGoogleVisionAPI(
        with base64EncodedImage: String,
        completion: @escaping ([String: Any]) -> Void) {
        let parameters: Parameters = [
            "requests": [
                [
                    "image": [
                        "content": base64EncodedImage
                    ],
                    "features": [
                        [
                            "type": "TEXT_DETECTION"
                        ]
                    ]
                ]
            ]
        ]
        let headers: HTTPHeaders = [
            "X-Ios-Bundle-Identifier": Bundle.main.bundleIdentifier ?? "",
            ]
        Alamofire.request(
            apiURL,
            method: .post,
            parameters: parameters,
            encoding: JSONEncoding.default,
            headers: headers)
            .responseJSON { response in
                
                if let json = response.result.value as? [String: Any]{
                    completion(json)
                }

        }
    }
    
    private func base64EncodeImage(_ image: UIImage) -> String? {
        return image.pngData()?.base64EncodedString(options: .endLineWithCarriageReturn)
    }
}
