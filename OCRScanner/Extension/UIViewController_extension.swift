//
//  UIViewController_extension.swift
//  OCRScanner
//
//  Created by Chính Trình Quang on 2/12/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import UIKit
extension UIViewController{
    func setNavigationBar(forTittle tittle : String) {
        self.navigationItem.title = tittle
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
    }
    func pushView(to viewController: UIViewController) -> Void {
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    func presentView(to viewController: UIViewController) -> Void {
        self.present(viewController, animated: true, completion: nil)
    }
    func resetNavController() -> Void {
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.isNavigationBarHidden = false
    }
}
