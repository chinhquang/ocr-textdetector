//
//  UIString_extension.swift
//  OCRScanner
//
//  Created by Chính Trình Quang on 2/13/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
