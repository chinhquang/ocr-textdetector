//
//  HistoryState.swift
//  OCRScanner
//
//  Created by Chính Trình Quang on 2/12/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import ReSwift

struct HistoryState: StateType {
    var histories: [HistoryItem] = [HistoryItem]()
    init (){
        histories = [HistoryItem]()
    }
}
struct SettingState : StateType {
    var setting : Settings = Settings()
    init (){
        setting = Settings()
    }
}
struct DetectObjectState : StateType {
    var objects : [DetectObject] = [DetectObject]()
    init (){
        objects  = [DetectObject]()
    }
}
