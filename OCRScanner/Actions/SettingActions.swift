//
//  SettingAction.swift
//  OCRScanner
//
//  Created by Chính Trình Quang on 2/13/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation

import UIKit
import ReSwift

// all of the actions that can be applied to the state


struct SettingActionUpdate: Action {
    var settings: Settings
}
