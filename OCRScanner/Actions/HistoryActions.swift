//
//  HistoryActions.swift
//  OCRScanner
//
//  Created by Chính Trình Quang on 2/12/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit
import ReSwift

// all of the actions that can be applied to the state
struct HistoryActionSetList: Action {
    var history: [HistoryItem]
}
struct HistoryActionAdd: Action {
    var historyItem: HistoryItem
}

struct HistoryActionDelete: Action {
    var historyItem: HistoryItem
}
