//
//  DetectObjectActions.swift
//  OCRScanner
//
//  Created by Chính Trình Quang on 2/15/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//
import Foundation

import UIKit
import ReSwift

// all of the actions that can be applied to the state


struct DetectObjectActionAdd: Action {
    var object: DetectObject
}
struct DetectObjectActionSetList: Action {
    var objects: [DetectObject]
}
struct DetectObjectActionRemove: Action {
    var index: Int
}
struct DetectObjectActionRemoveAll: Action {
    
}
