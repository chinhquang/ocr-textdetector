//
//  SettingDataStore.swift
//  OCRScanner
//
//  Created by Chính Trình Quang on 2/13/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation

import CoreData
import UIKit
class SettingDataStore{
    private static var settings: Settings = Settings(lang: "eng", fontSize: 15, is_enableHistory: true)
    static var sharedInstance = SettingDataStore()
    private init() {}
    func getSetting() -> Settings {
        return SettingDataStore.settings
    }
    func updateSetting(newSetting : Settings) -> Void {
        SettingDataStore.settings = newSetting
    }
    
}
