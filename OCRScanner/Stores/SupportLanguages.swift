//
//  SupportLanguages.swift
//  OCRScanner
//
//  Created by Chính Trình Quang on 2/13/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
class SupportLanguages{
    private static var languages_supported : [Language] = [Language(code: "eng", name: "English"), Language(code: "fra", name: "France"),Language(code: "dan", name: "Danish")]
    static var sharedInstance = SupportLanguages()
    private init() {}
    func getLangSelected(index : Int) -> Language {
        return SupportLanguages.languages_supported[index]
    }
    func getNumbersOfLanguage() -> Int {
        return SupportLanguages.languages_supported.count
    }
}
