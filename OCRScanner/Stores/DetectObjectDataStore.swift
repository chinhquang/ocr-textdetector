//
//  DetectTextDataStore.swift
//  OCRScanner
//
//  Created by Chính Trình Quang on 2/15/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//
import Foundation

import CoreData
import UIKit
class DetectObjectDataStore{
    private static var textStore = [DetectObject]()
    static var sharedInstance = DetectObjectDataStore()
    private init() {}
    func getListObject ()->[DetectObject]{
        return DetectObjectDataStore.textStore
    }
    func add (newValue : DetectObject)->Void{
        DetectObjectDataStore.textStore.append(newValue)
    }
    func removeAll() {
        DetectObjectDataStore.textStore.removeAll()
    }
    func remove(at : Int) {
        DetectObjectDataStore.textStore.remove(at: at)
    }
    func get(at index : Int)->DetectObject {
        return DetectObjectDataStore.textStore[index]
    }
}
