//
//  HistoryDataStore.swift
//  OCRScanner
//
//  Created by Chính Trình Quang on 2/12/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import CoreData
import UIKit
class HistoryDataStore{
    private static var historyDictionary: [String : HistoryItem] = [String : HistoryItem]()
    static var sharedInstance = HistoryDataStore()
    private init() {}
    
    func getHistoryList() -> [HistoryItem] {
        return Array(HistoryDataStore.historyDictionary.values)
    }
    func fetchHistoryFromCoreData (fromEntityName entityName : String){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        //let entity = NSEntityDescription.entity(forEntityName: "EmployeeData", in: context)
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        //request.predicate = NSPredicate(format: "age = %@", "12")
        request.returnsObjectsAsFaults = false
       
        do {
            let result = try context.fetch(request)
            if let results = result as? [NSManagedObject]{
                for data in results  {
                    
                    let detail = HistoryItem()
                    
                    if let t = data.value(forKey : "content") as? String{
                        detail.content = t
                        
                    }
                    if let t = data.value(forKey : "timeSaving") as? String{
                        detail.timeSaving = t
                        
                    }
                    if let id = data.value(forKey : "id") as? String{
                        detail.id = id
                        HistoryDataStore.historyDictionary[id] = detail
                    }
                    
                    
                }
            }
            
        } catch {
            
            
            print("Failed fetching data")
        }
        
    }
    func saveRecord (entityname : String, newHistory : HistoryItem){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: entityname, in: context)
        let newUser = NSManagedObject(entity: entity!, insertInto: context)
        
        
        newUser.setValue(newHistory.id, forKey: "id")
        newUser.setValue(newHistory.timeSaving, forKey: "timeSaving")
        newUser.setValue(newHistory.content, forKey: "content")
        
        do {
            
            try context.save()
            
        } catch {
            
            print("Failed saving")
        }
    }

    func deleteData(id: String, entityname : String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: entityname)
//        fetchRequest.predicate = NSPredicate.init(format: "id == \(id)")
        fetchRequest.predicate = NSPredicate(format: "id == %@", id)
        
        
        do {
            let results = try context.fetch(fetchRequest)
            if results.count > 0{
                for x in results {
                    let objectDelete = x as NSManagedObject
                    context.delete(objectDelete)
                    delete(ID: id)
                    do{
                        try context.save()
                    }catch{
                        print(error)
                    }
                }
                
                
            }
            
        }
        catch {
            print("error executing fetch request: \(error)")
        }
        
        
    }

    
    func get(historyID: String) -> HistoryItem? {
        return HistoryDataStore.historyDictionary[historyID]
    }
    func add(historyItem: HistoryItem) {
        guard let id = historyItem.id else {
            return
        }
        HistoryDataStore.historyDictionary[id] = historyItem
    }
   
    func update(historyItem: HistoryItem) {
        guard let id = historyItem.id else {
            return
        }
        HistoryDataStore.historyDictionary[id] = historyItem
    }
    func delete(ID: String) {
        HistoryDataStore.historyDictionary.removeValue(forKey: ID)
    }
}
//private static var productDictionary: [ String: Product] = [
//    "iphone": Product(id: "iphone", name: "iPhone", price: 600),
//    "ipad": Product(id: "ipad", name: "iPad", price: 900),
//    "macbook": Product(id: "macbook", name: "MacBook", price: 1500)
//]
//private init() {}
//static var sharedInstance = ProductDataStore()
//
//
//
//func getProductList() -> [Product] {
//    return Array(ProductDataStore.productDictionary.values).sorted { $0.name < $1.name }
//}
//
//func get(productID: String) -> Product? {
//    return ProductDataStore.productDictionary[productID]
//}
//
//func add(product: Product) {
//    ProductDataStore.productDictionary[product.id] = product
//}
//
//func update(product: Product) {
//    ProductDataStore.productDictionary[product.id] = product
//}
//
//func delete(productID: String) {
//    ProductDataStore.productDictionary.removeValue(forKey: productID)
//}
