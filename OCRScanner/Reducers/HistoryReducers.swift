//
//  HistoryReducers.swift
//  OCRScanner
//
//  Created by Chính Trình Quang on 2/12/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit

import ReSwift
func handleAction(action: Action, state: HistoryState?) -> HistoryState {
    
    // if no state has been provided, create the default state
    var state = state ?? HistoryState()
    
    switch action {
    case let action as HistoryActionSetList:
        state.histories = action.history
    case let action as HistoryActionAdd:
        state.histories.append(action.historyItem)
        
    case let action as HistoryActionDelete:
        if let index = state.histories.index(where: { $0.id == action.historyItem.id }) {
            state.histories.remove(at: index)
        }
    default:
        break
    }
    
    return state
}
