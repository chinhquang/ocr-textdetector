//
//  SettingReducers.swift
//  OCRScanner
//
//  Created by Chính Trình Quang on 2/13/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import UIKit

import ReSwift
func handleAction(action: Action, state: SettingState?) -> SettingState {
    
    // if no state has been provided, create the default state
    var state = state ?? SettingState()
    
    switch action {
    case let action as SettingActionUpdate:
        state.setting = action.settings
    default:
        break
    }
    
    return state
}
