//
//  DetectObjectReducers.swift
//  OCRScanner
//
//  Created by Chính Trình Quang on 2/15/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import UIKit

import ReSwift
func handleAction(action: Action, state: DetectObjectState?) -> DetectObjectState {
    
    // if no state has been provided, create the default state
    var state = state ?? DetectObjectState()
    
    switch action {
    case let action as DetectObjectActionSetList:
        state.objects = action.objects
    case let action as DetectObjectActionAdd:
        state.objects.append(action.object)
        
    case let action as DetectObjectActionRemove:
        state.objects.remove(at: action.index)
    case let action as DetectObjectActionRemoveAll:
        state.objects.removeAll()
    default:
        break
    }
    
    return state
}
